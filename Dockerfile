FROM docker

RUN apk add --no-cache py2-pip git openssh wget pv curl &&\
    pip install docker-compose
